FROM daocloud.io/library/ubuntu:18.04
WORKDIR /opt

RUN sed -i s@/archive.ubuntu.com/@/mirrors.aliyun.com/@g /etc/apt/sources.list \
&& apt-get update --fix-missing -y \
&& apt-get clean \
&& apt-get install -y python3 python3-pip python3-dev \
&& apt-get install -y mysql-server nginx libmysqlclient-dev \
&& apt-get install -y build-essential libssl-dev libffi-dev \
#&& apt-get install -y libxml2-dev libxslt1-dev zlib1g-dev \
&& pip3 install uwsgi --default-timeout=100 -i https://mirrors.aliyun.com/pypi/simple/ \
&& pip3 install mysqlclient --default-timeout=100 -i https://mirrors.aliyun.com/pypi/simple/ 

# create DB
RUN chown -R mysql:mysql /var/lib/mysql \
&& service mysql start \
&& mysql -uroot -e "CREATE DATABASE if not exists AugOps222 DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;"\
&& mysql -uroot -e "CREATE USER admin@127.0.0.1 IDENTIFIED BY 'abc123_'"\
&& mysql -uroot -e "GRANT ALL PRIVILEGES ON AugOps222.* TO 'admin'@'127.0.0.1'"
#&& mysql --one-database AugOps222 < /path/xxxxx.sql 

# copy dist, python project, ngix.config, etc
COPY . /opt

#virtualenv venv --python=python3.6

#sudo sh ./venv/bin/activate
#deactivate

RUN pip3 install -r ./requirements/requirements.txt --default-timeout=100 -i https://mirrors.aliyun.com/pypi/simple/ \
&& python3 manage.py makemigrations \
&& python3 manage.py migrate
#python3 createsuperuser.py

EXPOSE 80
EXPOSE 8000