"""
WSGI config for HelloWorld project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.0/howto/deployment/wsgi/
"""

import os
import sys

from django.core.wsgi import get_wsgi_application

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'HelloWorld.settings')

print(sys.path)

application = get_wsgi_application()

# sys.path.append('你的django项目路径')

# sys.path.append('python的site-packages路径')
# sys.path.insert(0, MORE_PATH)

# python 3
# sys.path.append('/usr/local/bin/python3.4')
# sys.path.append('/usr/local/lib/python3.8/site-packages')

# project
# sys.path.append('/www/django/releases/persistent/bsrs/python3')
# sys.path.append('/www/django/releases/persistent/bsrs/bsrs-django')
# sys.path.append('/www/django/releases/persistent/bsrs/bsrs-django/project')
# sys.path.append('/www/django/releases/persistent/bsrs/bsrs-django/project/project')
