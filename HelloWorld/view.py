from django.shortcuts import render
from django.http import HttpResponse


def hello(request):
    context = {'hello': 'Hello World!'}
    return render(request, 'hello.html', context)


def hello2(request):
    return HttpResponse('Hello World !')


def hello3(request):
    context = {'hello': 'Hello World!'}
    return render(request, 'hello3.html', context)
